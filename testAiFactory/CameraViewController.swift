//
//  CameraViewController.swift
//  testAiFactory
//
//  Created by Kirill Belyshev on 27/11/2018.
//  Copyright © 2018 itstorage. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class CameraViewController: UIViewController {
    
    // MARK: - Constants
    private struct Constants {
        static let filterSegueIdentifier = "FilterVC"
    }
    
    // MARK: - Properties
    let session = AVCaptureSession()
    let imageOutput = AVCapturePhotoOutput()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private var selectedImage: UIImage?
    
    @IBOutlet weak var cameraPreviewView: UIView!
    
    //TODO примишен
    override func viewDidLoad () {
        super.viewDidLoad()
        
        setupCamera()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == Constants.filterSegueIdentifier) {
            if let filtersVC = segue.destination as? FiltersViewController {
                filtersVC.originalImage = selectedImage
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoPreviewLayer?.frame = cameraPreviewView.bounds
    }
    
    func setupCamera() {
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video) else {
            //TODO сообщение об ошибке
            return
        }
        
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera)
        } catch let error {
            print(error.localizedDescription)
            //TODO сообщение об ошибке
        }
        
        session.sessionPreset = AVCaptureSession.Preset.photo
        if session.canAddInput(input) {
            session.addInput(input)
            if session.canAddOutput(imageOutput) {
                session.addOutput(imageOutput)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                guard let videoPreviewLayerExt = videoPreviewLayer else {
                    //TODO сообщение об ошибке
                    return
                }
                videoPreviewLayerExt.videoGravity = AVLayerVideoGravity.resizeAspect
                videoPreviewLayerExt.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                cameraPreviewView.layer.addSublayer(videoPreviewLayerExt)
                
                session.startRunning()
            }
        }
    }
    
    @IBAction func photoLibraryAction(_ sender: Any) {
        showPhotoLibrary()
    }
    
    @IBAction func didTapOnTakePhotoButton(_ sender: UIButton) {
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        imageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    func showPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func shownFilters(_ image: UIImage) {
        selectedImage = image
        performSegue(withIdentifier: Constants.filterSegueIdentifier, sender: self)
    }
}

extension CameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation() else { return }
        
        if let image = UIImage(data: imageData) {
            shownFilters(image)
        }
    }
}

extension CameraViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            shownFilters(image)
        } else {
            //TODO сообщение об ошибке
        }
        dismiss(animated: true, completion: nil)
    }
}
