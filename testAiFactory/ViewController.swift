//
//  ViewController.swift
//  testAiFactory
//
//  Created by Kirill Belyshev on 26/11/2018.
//  Copyright © 2018 itstorage. All rights reserved.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    
    // MARK: - Constants
    private struct Constants {
        static let filterSegueIdentifier = "FilterVC"
        static let cameraSegueIdentifier = "CameraVC"
    }
    
    // MARK: - Properties
    private var selectedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == Constants.filterSegueIdentifier) {
            if let filtersVC = segue.destination as? FiltersViewController {
                filtersVC.originalImage = selectedImage
            }
        }
    }
    @IBAction func firstVersion(_ sender: Any) {
        showActionSheet()
    }
    
    @IBAction func secondVersion(_ sender: Any) {
        performSegue(withIdentifier: Constants.cameraSegueIdentifier, sender: self)
    }
    
    func cameraPermission() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            showCamera()
        default:
            permissionPrimeCameraAccess()
        }
    }
    
    func permissionPrimeCameraAccess() {
        let deviceDescoverySession = AVCaptureDevice.DiscoverySession.init(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        if deviceDescoverySession.devices.count <= 0 {
            //TODO сообщение об ошибке
            return
        }
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [weak self] granted in
            DispatchQueue.main.async {
                self?.cameraPermission()
            }
        })
    }
    
    func showCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func showPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(
            title: "Camera",
            style: .default,
            handler: { [weak self] (alert:UIAlertAction!) -> Void in
                self?.cameraPermission()
            }
        ))
        
        actionSheet.addAction(UIAlertAction(
            title: "Gallery",
            style: .default,
            handler: { [weak self] (alert:UIAlertAction!) -> Void in
                self?.showPhotoLibrary()
            }
        ))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func shownFilters(image: UIImage) {
        selectedImage = image
        performSegue(withIdentifier: Constants.filterSegueIdentifier, sender: self)
    }
}

extension ViewController: UIImagePickerControllerDelegate {
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            shownFilters(image: image)
        } else {
            //TODO сообщение об ошибке
        }
        dismiss(animated: true, completion: nil)
    }
}

extension ViewController: UINavigationControllerDelegate {
    
}

