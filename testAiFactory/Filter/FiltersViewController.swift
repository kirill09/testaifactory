//
//  FiltersViewController.swift
//  testAiFactory
//
//  Created by Kirill Belyshev on 26/11/2018.
//  Copyright © 2018 itstorage. All rights reserved.
//

import Foundation
import UIKit

class FiltersViewController: UIViewController {
    
    // MARK: - Constants
    private struct Constants {
        static let filterCellIdentifier = "FilterCell"
        static let filterNames = [
            "", //originalImages
            "CIPhotoEffectChrome",
            "CIPhotoEffectFade",
            "CIPhotoEffectInstant",
            "CIPhotoEffectNoir",
            "CIPhotoEffectProcess",
            "CIPhotoEffectTonal",
            "CIPhotoEffectTransfer",
            "CISepiaTone"
        ]
    }
    
    // MARK: - Properties
    weak var originalImage: UIImage?
    
    // MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var filterCollection: UICollectionView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard originalImage != nil else {
            //TODO сообщение об ошибке
            return
        }
        imageView.image = originalImage
        
        filterCollection.delegate = self
        filterCollection.dataSource = self
    }
    
    @IBAction func shareAction(_ sender: Any) {
        guard let image = imageView.image else {
            //TODO сообщение об ошибке
            return
        }
        
        let imageToShare = [ image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        present(activityViewController, animated: true, completion: nil)
    }
}


extension FiltersViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.filterNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.filterCellIdentifier, for: indexPath)
        
        if let filterCell = cell as? FilterViewCell, let originalImage = originalImage {
            filterCell.setup(presenter: FilterPresenter(originalImage: originalImage, filter: Constants.filterNames[indexPath.item]))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? FilterViewCell else {
            //TODO сообщение об ошибке
            return
        }
        
        imageView.image = cell.modifiedImageView.image
    }
}
