//
//  FilterViewCell.swift
//  testAiFactory
//
//  Created by Kirill Belyshev on 27/11/2018.
//  Copyright © 2018 itstorage. All rights reserved.
//

import Foundation
import UIKit

class FilterViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var modifiedImageView: UIImageView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    func setup(presenter: FilterPresenter) {
        activity.isHidden = false
        activity.startAnimating()

        presenter.getFilterImage { [weak self] (modifiedImage) in
            self?.modifiedImageView.image = modifiedImage
            self?.activity.stopAnimating()
            self?.activity.isHidden = true
        }
    }
    
}
