//
//  FilterPresenter.swift
//  testAiFactory
//
//  Created by Kirill Belyshev on 05/02/2019.
//  Copyright © 2019 itstorage. All rights reserved.
//

import Foundation
import UIKit

class FilterPresenter {
    // MARK: - Constants
    private struct Constants {
        static let resizeWidth: CGFloat = 1000
        static let resizeHeight: CGFloat = 1000
    }
    
    private let originalImage: UIImage
    private let filterName: String
    
    var image: UIImage {
        get {
            if originalImage.size.width > Constants.resizeWidth
                || originalImage.size.height > Constants.resizeHeight {
                return resizeImage(image: originalImage, targetSize: CGSize(width: Constants.resizeWidth, height: Constants.resizeHeight)) ?? originalImage
            }
            
            return originalImage
        }
    }
    
    init(originalImage: UIImage, filter: String) {
        self.originalImage = originalImage
        self.filterName = filter
    }
    
    private func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    //TODO при больших размерах скорее всего ошибка по памяти
    //TODO
    func getFilterImage(completed: @escaping (UIImage) -> Void) {
        if (filterName.isEmpty) {
            completed(image)
            return
        }
        DispatchQueue.global(qos: .userInitiated).async {
            let filter = CIFilter(name: "\(self.filterName)" )
            filter?.setDefaults()
            filter?.setValue(CIImage(image: self.image), forKey: kCIInputImageKey)
            
            guard let filteredImageData = filter?.value(forKey: kCIOutputImageKey) as? CIImage,
                let filteredImageRef = CIContext(options: nil).createCGImage(filteredImageData, from: filteredImageData.extent) else {
                    //TODO сообщение об ошибке
                    return
            }
            
            DispatchQueue.main.async {
                completed(UIImage(cgImage: filteredImageRef))
            }
        }
    }
}
